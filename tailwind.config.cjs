module.exports = {
	content: ['./src/**/*.{html,js,svelte,ts}'],
	theme: {
		extend: {
			colors: {
				primary: '#1780d9',
				secondary: '#7ae582',
				bgLight: '#fbfbfb',
				textLight: '#00091c',
				bgDark: '#00091c',
				textDark: '#fff'
			}
		}
	},
	plugins: []
};
